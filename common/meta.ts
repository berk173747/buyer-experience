import {
  MetaPropertyCharset,
  MetaPropertyEquiv,
  MetaPropertyMicrodata,
  MetaPropertyName,
  MetaPropertyProperty,
  MetaInfo,
} from 'vue-meta/types/vue-meta';
import {
  DEFAULT_META_DESCRIPTION,
  DEFAULT_OPENGRAPH_IMAGE,
  META_NAME,
  SITE_URL,
  TWITTER_CARD_CONTENT,
  TWITTER_CREATOR_CONTENT,
  TWITTER_SITE_CONTENT,
} from '~/common/constants';

import createSchemaFaq from '~/common/craftSchema';

export function getPageMetadata({
  description = DEFAULT_META_DESCRIPTION,
  title,
  image_title: imageTitle,
  twitter_image: twitterImage,
  image_alt: imageAlt,
  schema_faq,
  schema_org,
}: // eslint-disable-next-line camelcase
{
  description: string;
  title: string;
  image_title: string;
  twitter_image: string;
  image_alt: string;
  schema_faq: [];
  schema_org: string;
}): MetaInfo {
  const meta: (
    | MetaPropertyCharset
    | MetaPropertyEquiv
    | MetaPropertyName
    | MetaPropertyMicrodata
    | MetaPropertyProperty
    | any
  )[] = [];
  let script: any = [];

  meta.push({
    hid: META_NAME.description,
    name: META_NAME.description,
    content: description,
  });
  meta.push({
    hid: META_NAME.twitterDescription,
    name: META_NAME.twitterDescription,
    content: description,
  });
  meta.push({
    hid: META_NAME.ogDescription,
    property: META_NAME.ogDescription,
    content: description,
  });

  meta.push({
    hid: META_NAME.ogTitle,
    property: META_NAME.ogTitle,
    content: title,
  });
  meta.push({
    hid: META_NAME.twitterCreator,
    name: META_NAME.twitterCreator,
    content: TWITTER_CREATOR_CONTENT,
  });
  meta.push({
    hid: META_NAME.twitterSite,
    name: META_NAME.twitterSite,
    content: TWITTER_SITE_CONTENT,
  });
  meta.push({
    hid: META_NAME.twitterCard,
    name: META_NAME.twitterCard,
    content: TWITTER_CARD_CONTENT,
  });

  meta.push({
    hid: META_NAME.twitterAltImage,
    name: META_NAME.twitterAltImage,
    content: imageAlt,
  });
  meta.push({
    hid: META_NAME.ogImageAlt,
    name: META_NAME.ogImageAlt,
    content: imageAlt,
  });

  if (title) {
    meta.push({
      hid: META_NAME.ogType,
      property: META_NAME.ogType,
      content: META_NAME.article,
    });
    meta.push({
      hid: META_NAME.twitterTitle,
      name: META_NAME.twitterTitle,
      content: title,
    });
  } else {
    meta.push({
      hid: META_NAME.ogType,
      content: META_NAME.website,
      property: META_NAME.ogType,
    });
  }

  if (imageTitle) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${imageTitle}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      name: META_NAME.ogImage,
      content: `${SITE_URL}${imageTitle}`,
    });
  } else if (twitterImage) {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${twitterImage}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      name: META_NAME.ogImage,
      content: `${SITE_URL}${twitterImage}`,
    });
  } else {
    meta.push({
      hid: META_NAME.twitterImage,
      name: META_NAME.twitterImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
    meta.push({
      hid: META_NAME.ogImage,
      name: META_NAME.ogImage,
      content: `${SITE_URL}${DEFAULT_OPENGRAPH_IMAGE}`,
    });
  }

  if (schema_faq) {
    script.push({
      hid: 'schemaFaq',
      json: createSchemaFaq(schema_faq),
      type: 'application/ld+json',
    });
  }

  if (schema_org) {
    script.push({
      hid: 'schemaOrg',
      innerHTML: schema_org,
      type: 'application/ld+json',
    });
  }

  return {
    title,
    meta,
    script,
    __dangerouslyDisableSanitizers: ['script', 'innerHTML'],
  };
}
