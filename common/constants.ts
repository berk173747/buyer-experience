export const DEFAULT_META_DESCRIPTION =
  'Learn more from GitLab, The One DevOps Platform for software innovation.';
export const TWITTER_CREATOR_CONTENT = '@Gitlab';
export const TWITTER_SITE_CONTENT = '@Gitlab';
export const TWITTER_CARD_CONTENT = 'summary_large_image';
export const DEFAULT_OPENGRAPH_IMAGE =
  '/nuxt-images/open-graph/open-graph-gitlab.png';
export const SITE_URL = 'https://about.gitlab.com';

export enum META_NAME {
  description = 'description',
  ogDescription = 'og:description',
  twitterDescription = 'twitter:description',
  twitterTitle = 'twitter:title',
  twitterAltImage = 'twitter:image:alt',
  twitterImage = 'twitter:image',
  twitterCreator = 'twitter:creator',
  twitterCard = 'twitter:card',
  twitterSite = 'twitter:site',
  ogImage = 'og:image',
  ogImageAlt = 'og:image:alt',
  ogTitle = 'og:tittle',
  article = 'article',
  website = 'website',
  ogType = 'og:type',
}

export const RESOURCE_ICON_PATH = Object.freeze({
  empty_icon: '',
  case_study: '/nuxt-images/icons/icon-case-study.svg',
  article: '/nuxt-images/icons/icon-articles.svg',
  book: '/nuxt-images/icons/icon-book.svg',
  blog: '/nuxt-images/icons/icon-blog.svg',
  podcast: '/nuxt-images/icons/icon-podcast.svg',
  report: '/nuxt-images/icons/icon-report.svg',
  video: '/nuxt-images/icons/icon-video.svg',
  webcast: '/nuxt-images/icons/icon-webcast.svg',
  whitepaper: '/nuxt-images/icons/icon-whitepapers.svg',
  next_step: '/nuxt-images/icons/icon-next-steps.svg',
  gartner_logo: '/nuxt-images/logos/gartner_logo.svg',
  forrester_logo: '/nuxt-images/logos/forrester_logo.svg',
  idc_logo: '/nuxt-images/logos/idc_logo.svg',
});

export enum FEATURES_HOSTING {
  saas = 'SaaS',
  self_managed = 'Self-Managed',
}

export enum TEMPLATES {
  Default = 'default',
  Industry = 'industry',
}
