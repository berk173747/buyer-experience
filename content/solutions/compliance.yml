---
  title: Continuous Software Compliance with GitLab
  description: How to use GitLab to build compliant applications with a secure software supply chain.
  components:
    - name: 'solutions-hero'
      data:
        title: Continuous Software Compliance with GitLab
        subtitle: Building applications that meet common regulatory standards with a secure software supply chain
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Join GitLab
          url: /free-trial/
          # TODO: Change for actual image
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg          
          alt: "Image: continuous software compliance with gitLab"
    - name: 'copy-media'
      data:
        block:
          - header: Continuous Software Compliance
            miscellaneous: |
              Software compliance is no longer just about checking boxes. Cloud native applications present entirely new attack surfaces via containers, orchestrators, web APIs, and other infrastructure-as-code. These new attack surfaces, along with complex DevOps toolchains have resulted in notorious software supply chain attacks and led to new regulatory requirements. Continuous software compliance is becoming a critical way to manage risk inherent in Cloud Native applications and DevOps automation - beyond merely reducing security flaws within the code itself.

              DevOps automation requires a new level of sophistication to monitor and protect what has become the modern software factory. Continuous software compliance can be difficult when it is disconnected from the software development process. Organizations need a compliance program that is built-in, not bolted-on, to their existing workflows and processes. "Traditional compliance practices are incompatible with continuous software delivery processes, leading to slower delivery and unexpected, expensive remediation work." ([Gartner®, Hype Cycle™ for Agile and DevOps, 2021, Herschmann, Joachim and Spafford, George, 2021](https://www.gartner.com/document/4003540))

              **Learn more by downloading the [Guide to Software Supply Chain Security](https://page.gitlab.com/resources-ebook-modernsoftwarefactory.html)**


    - name: copy-media
      data:
        block:
          - header: How GitLab simplifies Continuous Software Compliance
            text: |
              GitLab's [compliance management](https://docs.gitlab.com/ee/administration/compliance.html){data-ga-name="compliance management" data-ga-location="body"} capabilities aim to create an experience that's simple, friendly, and as frictionless as possible by enabling you to define, enforce and report on compliance policies and frameworks. Our platform approach simplifies the effort.

              As a complete DevOps platform, GitLab is a great choice for compliance teams to provide compliance guardrails that allow rapid software development while ensuring compliance is integrated into development and deployment processes early, in a manner somewhat similar to the movement to shift security left.
            miscellaneous: |
              *   **Policy Management** helps you define rules and policies to adhere to compliance frameworks and common controls

              *   **Compliant Workflow Automation** helps you enforce the defined rules and policies

              *   **Audit Management** helps you log activities throughout your DevOps automation to identify incidents and prove adherence to compliance rules and defined policies. Visibility is greater with one platform and no toolchain silos.

              *   **Security testing and vulnerability management** helps you ensure [security scanning and license compliance](/solutions/dev-sec-ops/){data-ga-name="dev sec ops" data-ga-location="body"} for every code change and allows DevOps engineers and Security Pros alike to track and manage vulnerabilities.

              *  **Software Supply Chain Security** helps you manage the end-to-end attack surfaces of Cloud Native applications and DevOps automation - [beyond traditional application security testing](/direction/supply-chain/).
    - name: 'copy-media'
      data:
        block:
          - header: Policy Management
            text: |
              [Policy Management](https://docs.gitlab.com/ee/administration/compliance.html#policy-management) helps you define rules and policies to adhere to - either internal company policies or policies based on legal or regulatory frameworks such as GDPR, SOC2, PCI-DSS, SOX, HIPAA, ISO, COBIT, FedRAMP, and so on.

              *   **Granular User Roles and Permissions** GitLab supports multiple different user roles with permissions according to the user's role, rather than access required to a repository.

              *   **Compliance Settings** Define and enforce compliance policies on specific projects, groups, and users.

              *   **Credentials Inventory** Keep track of all the credentials that can be used to access the GitLab self-managed instance.

              *   **Protected Branches** Control unauthorized modifications to specific branches - including creating, pushing, deleting of a branch - without adequate permissions or approvers.
            image:
              image_url: /nuxt-images/solutions/compliance/compliance-policy-mgmt.png
              alt: " "
          - header: Compliant Workflow Automation
            inverted: true
            text: |
              GitLab enables powerful [compliance automation](https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation) through enforcing policies and separation of duties while reducing overall business risk.

              *   **Granular User Roles and Permissions** GitLab supports multiple different user roles with permissions according to the user's role, rather than access required to a repository.

              *   **Compliance Framework Project Templates** Create projects that map to specific audit protocols such as HIPAA - to help maintain an audit trail & manage compliance programs. Templates are managed by admins to ensure consistent and compliant use.

              *   **Compliance Framework Pipelines** Define compliance jobs that should be run in every pipeline to ensure that security scans are run, artifacts are created and stored, or any other steps required by your organizational requirements. These jobs are automatically merged with the project's jobs, enabling developers to stay focused on adding business value while still remaining compliant.

            image:
              image_url: /nuxt-images/solutions/compliance/compliance-frameworks.png
              alt: " "
          - header: Audit Management
            text: |
              Audits require traceability of compliance events to show what happened, when it happened, and who did it. GitLab's robust [audit event system](https://docs.gitlab.com/ee/administration/compliance.html#audit-management) records this for the most important actions and provides mulitple ways to consume the results.

              *   **Compliance Framework Project Labels** Clearly delineate which projects need certain compliance controls and which do not. Easily apply common compliance settings to a project with a label.

              *   **Audit Events** Aims to satisfy organizational audit logging requirements within the UI or via API.

              *   **Compliance Dashboard** Provide compliance insights in a consolidated view with all relevant compliance signals such as segregation of duties, framework compliance, license compliance, pipeline and MR results. Currently, the dashboard focuses on most recently merged MR activity.
            image:
              image_url: /nuxt-images/solutions/compliance/compliance-audit-mgmt.png
              alt: " "
    - name: copy-media
      data:
        block:
          - header: Security scanning and vulnerability management with Gitlab
            text: |
              GitLab's approach to [DevSecOps](/solutions/dev-sec-ops/) directly integrates required compliance jobs into developer pipelines, ensures proper seperation of duties, audit systems, and more This makes it possible to implement a 'shift-left' approach for both security and compliance.

              **Visit the [DevSecOps page](/solutions/dev-sec-ops/){data-ga-name="dev sec ops" data-ga-location="body"} to learn more.**
            miscellaneous: |
              *   **Vulnerability scanning** automatically applies comprehensive scan types at every code commit and upon merge

              *   **License compliance scanning** automatically looks for unapproved use of third party code with results alongside security scans

              *   **MR approvals** allows you to determine if approvals are required for severe vulnerabilities

              *   **Vulnerability management** helps you assess risk, triage unresolved vulnerabilities, and track their remediation.

    - name: 'solutions-feature-list'
      data:
        title: Protecting your software supply chain is challenging.
        subtitle: GitLab Continous Software Compliance capabilities help organizations adhere to policies and regulatory requirements while ensuring development velocity, providing necessary simplicity, visibility, and control.
        icon: /nuxt-images/enterprise/gitlab-enterprise-icon-tanuki.svg
        features:
          - title: Visibility and control
            description: |
              Audits require traceability of compliance events to show what happened, when it happened, and who did it. GitLab's robust audit event system records this for the most important actions and provides mulitple ways to consume the results. Easily apply common compliance settings to a project with a label.

              *   [PCI Compliance](/solutions/pci-compliance/){data-ga-name="pci compliance" data-ga-location="body"}
              *   [HIPAA Compliance](/solutions/hipaa-compliance/){data-ga-name="hipaa compliance" data-ga-location="body"}
              *   [Financial Services Regulatory Compliance](/solutions/financial-services-regulatory-compliance/){data-ga-name="finalcial services regulatory compliance" data-ga-location="body"}
              *   [GDPR](/gdpr){data-ga-name="gdpr" data-ga-location="body"}
              *   [IEC 62304:2006](/solutions/iec-62304/){data-ga-name="iec 62304" data-ga-location="body"}
              *   [ISO 13485:2016](/solutions/iso-13485/){data-ga-name="iso 13285" data-ga-location="body"}
              *   [ISO 26262-6:2018](/solutions/iso-26262/){data-ga-name="iso 26262" data-ga-location="body"}
              *   Your own policy framework
            icon: /nuxt-images/icons/slp-cicd.svg
          - title: "Customer story: Chorus.ai"
            description: |
              During a recent audit for SOC2 compliance, the auditors said that Chorus had the fastest auditing process they have seen and most of that is due to the capabilities of GitLab.



              **Russell Levy**


              Co-Founder, and Chief Technology Officer, Chorus.ai


              [Read full story](/customers/chorus/){data-ga-name="read full story" data-ga-location="body"}
            icon: /nuxt-images/icons/slp-sourcecode.svg
          - title: Help and More Information
            description: |
              *   Please see [Get help for GitLab](/get-help/){data-ga-name="get help" data-ga-location="body"} if you have questions
              *   Docs: [Compliance Features](https://docs.gitlab.com/ee/administration/compliance.html){data-ga-name="compliance features" data-ga-location="body"}
              *   Docs: [Compliance Dashboard](https://docs.gitlab.com/ee/user/compliance/compliance_dashboard){data-ga-name="compliance dashboard" data-ga-location="body"}
              *   Demo: [Compliant pipelines](https://www.youtube.com/watch?v=jKA_e_jimoI){data-ga-name="compliant pipelines demo" data-ga-location="body"}
              *   Blog: [Three things you may not know about GitLab security](/blog/2021/11/23/three-things-you-might-not-know-about-gitlab-security/)
              *   Webinar: [Secure your software supply chain](https://www.youtube.com/watch?v=dZfS4Wt5ZRE)
              *   Roadmaps [Audit Events](/direction/manage/audit-events/){data-ga-name="audit events" data-ga-location="body"} | [Audit Reports](/direction/manage/audit-reports/){data-ga-name="audit reports" data-ga-location="body"} | [Compliance Management](/direction/manage/compliance-management/){data-ga-name="compliance management" data-ga-location="body"} | [Authentication & Authorization](/direction/manage/auth/){data-ga-name="authentication" data-ga-location="body"} | [User Management](/direction/manage/users/){data-ga-name="user management" data-ga-location="body"}
            icon: /nuxt-images/icons/icon-cog.svg
    - name: 'slp-disclaimer'
      data:
        text: |
          GARTNER and HYPE CYCLE are registered trademark and service mark of Gartner, Inc. and/or its affiliates in the U.S. and internationally and are used herein with permission. All rights reserved..

