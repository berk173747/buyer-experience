---
  package:
    content: Package
    copy_media:
      header: Package
      aos_animation: fade-up
      aos_duration: 500
      icon_url: /nuxt-images/icons/devops/package-colour.svg
      subtitle: Create a consistent and dependable software supply chain with built-in package management.
      text: GitLab enables teams to package their applications and dependencies, manage containers, and build artifacts with ease. The private, secure, container and package registry are built-in and preconfigured out-of-the box to work seamlessly with GitLab source code management and CI/CD pipelines. Ensure DevOps acceleration and a faster time to market with automated software pipelines that flow freely without interruption.
      categories:
        - Package Registry
        - Container Registry
        - Helm Chart Registry
        - Dependency Proxy
        - Dependency Firewall
  feature_content:
    products_category:
      products:
        # Package Registry
        - title: Conan (C/C++) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Conan is an open source, decentralized and multi-platform C/C++ Package Manager for developers to create and share native binaries.
          link:
            href: https://docs.gitlab.com/ee/user/packages/conan_repository/
            text: Documentation
            data_ga_name: Conan (C/C++) Repository
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Maven (Java) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab's Maven repository makes it easier to publish and share Java libraries across an organization, and ensure dependencies are managed correctly. It is fully integrated with GitLab, including authentication and authorization.
          link:
            href: https://docs.gitlab.com/ee/user/packages/maven_repository/index.html
            text: Documentation
            data_ga_name: Maven (Java) Repository
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: npm (node) Registry
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab's NPM repository makes it easier to publish and share NPM packages across an organization, and ensure dependencies are managed correctly. It is fully integrated with GitLab, including authentication and authorization.
          link:
            href: https://docs.gitlab.com/ee/user/packages/npm_registry/index.html
            text: Documentation
            data_ga_name: npm (node) Registry
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: NuGet (.NET) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab's NuGet Repository allows C#/.NET developers to create, publish and share packages using the NuGet client or visual studio.
          link:
            href: https://docs.gitlab.com/ee/user/packages/nuget_repository/
            text: Documentation
            data_ga_name: NuGet (.NET) Repository
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Composer (PHP) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: This feature helps PHP developers to build, publish and share their packages right alongside their source code and pipelines.
          link:
            href: https://docs.gitlab.com/ee/user/packages/composer_repository/
            text: Documentation
            data_ga_name: Composer (PHP) Repository
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Use the Package Registry through REST API
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Enables support for automation and integration of the GitLab Package Registry through a REST API.
          link:
            href: https://docs.gitlab.com/ee/api/packages.html
            text: Documentation
            data_ga_name: Use the Package Registry through REST API
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Forward requests for npm packages not found in GitLab to npmjs.com
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: By default, when an npm package is not found in the GitLab registry, the request is forwarded to npmjs.com
          link:
            href: https://docs.gitlab.com/ee/user/packages/npm_registry/#forwarding-requests-to-npmjsorg
            text: Documentation
            data_ga_name: Forward requests for npm packages not found in GitLab to npmjs.com
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Forward requests for Python packages not found in GitLab to PyPI.org
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: By default, when a PyPI package is not found in the GitLab registry, the request is forwarded to PyPI.org
          link:
            href: https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html#pypi-forwarding
            text: Documentation
            data_ga_name: Forward requests for Python packages not found in GitLab to PyPI.org
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: PyPI (Python) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Python developers can set up GitLab as a remote PyPI repository and build, publish, and share packages using the PyPI client or GitLab CI/CD.
          link:
            href: https://docs.gitlab.com/ee/user/packages/pypi_repository/
            text: Documentation
            data_ga_name: PyPI (Python) Repository
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Go Proxy
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: This feature helps Go developers to publish and share their packages right alongside their source code and pipelines. This will also be a valuable feature for GitLab and help with [dogfooding](/handbook/values/#dogfooding)
          link:
            href: https://docs.gitlab.com/ee/user/packages/go_proxy/
            text: Documentation
            data_ga_name: Go Proxy
            data_ga_location: body
          saas: []
          self_managed:
            - free
            - premium
            - ultimate
        - title: Package debugging with an integrated web terminal
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Easily debug your packages in any of your environments using the built-in GitLab Web Terminal. GitLab can open a terminal session directly from your environment if your application is deployed on Kubernetes. This is a very powerful feature where you can quickly debug issues without leaving the comfort of your web browser.
          link:
            href: https://docs.gitlab.com/ee/ci/environments/index.html#web-terminals
            text: Documentation
            data_ga_name: Package debugging with an integrated web terminal
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Publish and share package versions
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Each version of a package is nested under its uniquely-named parent. Now you can easily find the package you are looking for in the UI and better understand what has changed from version to version.
          link:
            href: https://docs.gitlab.com/ee/user/packages/
            text: Documentation
            data_ga_name: Publish and share package versions
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Generic Package Registry
          categories:
            - Package Registry
            - Release Orchestration
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab supports a wide variety of languages in our [Package Registry](https://docs.gitlab.com/ee/user/packages/) offering. However, you may want to store other binary types in GitLab that are not yet supported. GitLab supports raw package feeds (like you could do in Nexus) to a Generic Package Registry. Looking forward, this feature helps create the foundation for [Release Assets](https://gitlab.com/groups/gitlab-org/-/epics/2207) and will ultimately make it easier for you to package and release your software with GitLab.
          link:
            href: https://docs.gitlab.com/ee/user/project/releases/#release-assets
            text: Documentation
            data_ga_name: Generic Package Registry
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Container Registry
        - title: Built-in Container Registry
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab Container Registry is a secure and private registry for Docker images. It allows for easy upload and download of images from GitLab CI. It is fully integrated with Git repository management. (Codefresh will be ending their support for private docker registries as of May 1, 2020
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/index.html
            text: Documentation
            data_ga_name: Built-in Container Registry
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Built for using containers and Docker
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab ships with its own Container Registry, Docker CI Runner, and is ready for a complete CI/CD container workflow. There is no need to install, configure, or maintain additional plugins.
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/
            text: Documentation
            data_ga_name: Built for using containers and Docker
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Docker image support
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Supports storage and retrieval of Docker style containers.
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/index.html
            text: Documentation
            data_ga_name: Docker image support
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Container registry webhooks
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Trigger actions after a successful push to a registry to integrate Docker Hub with other services.
          link:
            href: https://docs.gitlab.com/ee/administration/packages/container_registry.html#configure-container-registry-notifications
            text: Documentation
            data_ga_name: Container registry webhooks
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Container registry high availability
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Highly available through the use of multiple replicas of all containers and metadata such that if a machine fails, the registry continues to operate and can be repaired.
          link:
            href: https://docs.gitlab.com/ee/administration/reference_architectures/index.html
            text: Documentation
            data_ga_name: Container registry high availability
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Container Registry geographic replication
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Supports distributed teams by running multiple registry instances across several regions and syncing between data centers.
          link:
            href: https://docs.gitlab.com/ee/administration/geo/index.html
            text: Documentation
            data_ga_name: Container Registry geographic replication
            data_ga_location: body
          saas: []
          self_managed:
            - premium
            - ultimate
        - title: Supports private container registries
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Offers the ability to have private container registries and repositories
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/index.html#using-with-private-projects
            text: Documentation
            data_ga_name: Supports private container registries
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: SaaS container registry offering
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: The container registry is available as a software service.
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/index.html
            text: Documentation
            data_ga_name: SaaS container registry offering
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Use container registry through REST API
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Enables support for automation and integration of container registry through a REST API.
          link:
            href: https://docs.gitlab.com/ee/api/container_registry.html
            text: Documentation
            data_ga_name: Use container registry through REST API
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Container registry storage management
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: In the context of the Docker registry, garbage collection is the process of removing blobs from the filesystem when they are no longer referenced by a manifest.
          link:
            href: https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-garbage-collection
            text: Documentation
            data_ga_name: Container registry storage management
            data_ga_location: body
          saas: []
          self_managed:
            - free
            - premium
            - ultimate
        - title: Group-level Docker registry browser
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: A single UI view into images across multiple repositories.
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/
            text: Documentation
            data_ga_name: Group-level Docker registry browser
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Use search to find and container images
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Search your group and project's Container Registry by image name
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/#control-container-registry-from-within-gitlab
            text: Documentation
            data_ga_name: Use search to find and container images
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Cloud Native
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: GitLab and its CI/CD is Cloud Native, purpose built for the cloud model. GitLab can be easily deployed on Kubernetes and used to deploy your application to Kubernetes with support out of the box.
          link:
            href: /solutions/kubernetes/
            text: Documentation
            data_ga_name: Cloud Native
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Container debugging with an integrated web terminal
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Easily debug your containers in any of your environments using the built-in GitLab Web Terminal. GitLab can open a terminal session directly from your environment if your application is deployed on Kubernetes. This is a very powerful feature where you can quickly debug issues without leaving the comfort of your web browser.
          link:
            href: https://docs.gitlab.com/ee/ci/environments/index.html#web-terminals
            text: Documentation
            data_ga_name: Container debugging with an integrated web terminal
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Container image cleanup policies
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Easily define, manage and update project-level policies to define which images should be removed and preserved. This feature is designed to help you reduce storage costs and prevent important images from being deleted.
          link:
            href: https://docs.gitlab.com/ee/user/packages/container_registry/#cleanup-policy
            text: Documentation
            data_ga_name: Container image cleanup policies
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Global Docker registry browser
          categories:
            - Container Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: A single UI view into images across multiple repositories.
          link:
            href: https://gitlab.com/gitlab-org/gitlab/issues/23315
            text: Documentation
            data_ga_name: Global Docker registry browser
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Helm Chart Registry
        - title: Helm chart repository support
          categories:
            - Helm Chart Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Supports storage and retrieval of Helm charts.
          link:
            href: https://docs.gitlab.com/ee/user/packages/helm_repository/
            text: Documentation
            data_ga_name: Helm chart repository support
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # Dependency Proxy
        - title: Dependency Proxy for Container Registry
          categories:
            - Dependency Proxy
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: For many organizations, it is useful to have a caching proxy for frequently used upstream images/packages. In the case of CI/CD, the proxy is responsible for receiving a request and returning the upstream image from a registry, acting as a pull-through cache to speed up your pipelines. By keeping a copy of needed container layers locally, you can improve performance particularly for commonly used images, such as build environments.
          link:
            href: https://docs.gitlab.com/ee/user/packages/dependency_proxy/index.html
            text: Documentation
            data_ga_name: Dependency Proxy for Container Registry
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        # PACKAGE - MISSING FEATURES
        # Package Registry - Missing
        - title: RPM (Linux) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: This planned feature will enable Linux developers to build, publish and share RPM packages alongside their source code and pipelines.
          link:
            href: https://gitlab.com/gitlab-org/gitlab/issues/5932
            text: Issue details
            data_ga_name: RPM (Linux) Repository
            data_ga_location: body
        - title: Debian (Linux) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: This planned feature will enable Linux developers to build, publish and share Debian packages alongside their source code and pipelines.
          link:
            href: https://gitlab.com/gitlab-org/gitlab/issues/5835
            text: Issue details
            data_ga_name: Debian (Linux) Repository
            data_ga_location: body
        - title: RubyGems (Ruby) Repository
          categories:
            - Package Registry
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: This planned feature will enable Ruby developers to setup GitLab as a remote RubyGems repository and to build, publish and share packages using the command line or GitLab CI/CD. This will also be a valuable feature for GitLab and help with [dogfooding](/handbook/values/#dogfooding)
          link:
            href: https://gitlab.com/gitlab-org/gitlab/issues/803
            text: Issue details
            data_ga_name: RubyGems (Ruby) Repository
            data_ga_location: body
        # Dependency Proxy - Missing
        - title: Virtual registries
          categories:
            - Dependency Proxy
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: A virtual registry is a collection of local, remote and other virtual registries accessed through a single logical URL.
          link:
            href: https://gitlab.com/groups/gitlab-org/-/epics/2920
            text: Issue details
            data_ga_name: Virtual registries
            data_ga_location: body
        # Dependency Firewall - Missing
        - title: Dependency Firewall
          categories:
            - Dependency Firewall
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: For organizations that rely on many open source dependencies, it is important to limit their exposure to open source security vulnerabilities. This can be done by establishing and enforcing policies to restrict which dependencies may be used, by having a central location to verify the integrity of dependencies and how they are being utilized.
          link:
            href: /direction/package/dependency_firewall/
            text: Learn more
            data_ga_name: Dependency Firewall
            data_ga_location: body

