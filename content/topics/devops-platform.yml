---
  title: What is a DevOps platform? | GitLab
  description:  A DevOps platform brings tools together in a single application so everyone involved in the software development process -- from a product manager to an ops pro -- can seamlessly work together to release software faster.
  components:
    - name: topics-header
      data:
        title: DevOps platform
        block:
            - metadata:
                id_tag: devops-platform
              text: |
                  A DevOps platform combines the ability to develop, secure, and operate software in a single application so everyone involved in the software development process -- from a product manager to an ops pro -- can seamlessly work together to release software faster.
              resources:
                topic:
                  header: More on this topic
                  links:
                    - text: Test your DevOps platform knowledge
                      link: /quiz/devops-platform/
                      data_ga_location: header
                      data_ga_name: Test your DevOps platform knowledge
                    - text: Why you need a DevOps platform team
                      link: /topics/devops/how-and-why-to-create-devops-platform-team/
                      data_ga_location: header
                      data_ga_name: Why you need a DevOps platform team
                    - text: Reduce the cost of a DevOps platform
                      link: /topics/devops/reduce-devops-costs/
                      data_ga_location: header
                      data_ga_name: Reduce the cost of a DevOps platform
                    - text: What is a DevOps platform engineer?
                      link: /topics/devops/what-is-a-devops-platform-engineer/
                      data_ga_location: header
                      data_ga_name: What is a DevOps platform engineer?

    - name: 'copy'
      data:
        block:
          - header: What is a DevOps platform?
            column_size: 8
            text: |
              A [DevOps platform](/solutions/devops-platform/){:data-ga-name="Devops platform"}{:data-ga-location="body"} combines the ability to develop, secure, and operate software in a single application. A DevOps platform empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. Every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency, and traceability.


              On the surface, [DevOps](/topics/devops/){:data-ga-name="Devops"}{:data-ga-location="body"} brings devs and ops together; the reality, however, is quite a bit more complex as security, testers, product managers, Product Designers, finance, the legal team, and even marketing all have a hand to play when it comes to creating and releasing software. A DevOps platform gives all of the players involved a single place to communicate, collaborate, gather data, and analyze results – there’s no more hunting around for information or being left out of the loop.


              A DevOps platform also eliminates all the issues surrounding toolchain sprawl, maintenance, and integration. A platform provides a single source of truth, which streamlines the technical development process dramatically.

              ## Features of a DevOps platform

              By moving to a unified DevOps platform, teams can see what is happening, what needs to happen, what is going wrong, and more importantly how and where to intervene.
              A DevOps platform has 10 primary must-have features:

              * **Metrics/visibility:** A complete DevOps platform allows teams to optimize software delivery by giving them visibility and supporting data around the entire value stream.
              * **Planning:** No matter the methodology (from waterfall to [Agile](/solutions/agile-delivery/) or Kanban), planning is key, a DevOps platform provides flexible portfolio planning tools.
              * **Distributed version control:** The most efficient way to create software is through a single, distributed-version control system that can scale and iterate to support your business needs.
              * **Automated testing with integrated feedback:** There is no such thing as too much testing, a DevOps platform supports automated testing and provides developers the ability to achieve maximum results within their workspace (IDE) of choice.
              * **Package management:** Apps, their many dependencies, and containers require management as part of modern software development.
              * **Built-in security:** Anything that can streamline security is critical in today’s breach-filled world. SAST and DAST scans, dependency and container scanning, are all essential.
              * **Automated CD:** Teams want to get software out the door as quickly as possible, so a DevOps platform needs automated continuous delivery onboard and ready to go.
              * **Flexible infrastructure:** DevOps often requires teams to pivot quickly, having a configurable infrastructure – preferably one seamlessly tied into Kubernetes – is a key requirement in a DevOps platform.
              * **Incident management:** Problems arise regularly, a DevOps platform should offer complete visibility with fast and flexible incident management.
              * **Future-proofing:** A DevOps platform needs to work seamlessly with cutting-edge technologies like containers, microservices, cloud-native solutions, and artificial intelligence or machine learning.

    - name: benefits-icons
      data:
        title: Benefits of a DevOps platform
        horizontal_rule: true
        column_size: 8
        benefits:
          - subtitle: From code reviews to automated testing, incident management and monitoring, using a unified platform means every single part of DevOps is streamlined and, in some cases, actually doable for the first time.
            title: Ease of use
            icon: /nuxt-images/icons/computer.svg
          - title: Better collaboration
            subtitle: Business partners can actually see what’s going on with software development, release cycles, and customer feedback all in one place. Software teams will have [fewer miscommunications](/blog/2020/11/23/collaboration-communication-best-practices/){:data-ga-name="Fewer miscomunications"}{:data-ga-location="body"}, too.
            icon: /nuxt-images/icons/collaboration-icon.svg
          - subtitle: More testing, baked earlier into the process, means improved
              security, faster releases, and improved customer satisfaction.
            title: Safer code
            icon: /nuxt-images/icons/shield-checkmark.svg
          - title: Tighter feedback loops
            subtitle: Visibility and [traceability](/blog/2020/01/30/insights/) are the
              hallmarks of a DevOps platform because everything is in one place.
              Troubleshooting has never been easier.
            icon: /nuxt-images/icons/agile.svg
          - title: Performance monitoring
            subtitle: A DevOps platform allows teams to stop guessing at how software
              will work and actually see real world results.
            icon: /nuxt-images/icons/trending-icon2.svg
          - title: Fewer compliance headaches
            subtitle: Move to a DevOps platform and suddenly all of those things that had
              to be tracked and recorded will be handled automatically.
            icon: /nuxt-images/icons/computer-test.svg
          - title: Less technical debt
            subtitle: It’s easy to get even non-developers on board with reducing
              technical debt when everyone can see the burden it imposes, thanks to a
              single DevOps platform.
            icon: /nuxt-images/icons/piggy-bank.svg
          - title: Save time, save money
            subtitle: "A DevOps platform saves teams time (fewer tools to integrate,
              update, and maintain) and money (fewer tools to purchase, period). "
            icon: /nuxt-images/icons/gitops-benefits-costs.png
    - name: 'copy'
      data:
        block:
          - header: Get ready for a DevOps platform
            column_size: 8
            text: |
                Want to make sure your team is ready to get the most out of a DevOps platform? Here are seven things to consider before you begin:


                1. Do you _really_ understand your team’s *workflow*? You won’t get the biggest benefit from a DevOps platform if you don’t set it up to reflect the reality of how your team operates. There’s no right or wrong here.

                2. *Culture* matters and that’s particularly true when it comes to rolling out a DevOps platform. Make sure your messaging about a new DevOps platform will resonate with your team and organizational culture.

                3. It’s all about *deployments*, so it makes sense to understand exactly how your team [deploys](/blog/2020/07/23/safe-deploys/){:data-ga-name="Deploys"}{:data-ga-location="body"} now, what the hiccups are, and how you plan to address them before you add a DevOps platform to the mix.

                4. *Security* is top of mind for many teams, and a DevOps platform can make that even easier. Make sure to assess where your team is today, and what the goals are, and set simple goals to achieve. A DevOps platform can streamline security but the best way to proceed is through what we call [iteration](/blog/2020/02/04/power-of-iteration/){:data-ga-name="Iteration"}{:data-ga-location="body"} or small changes.

                5. A DevOps platform is a great opportunity to add in some *advanced technology* like [machine learning](/blog/2020/12/01/continuous-machine-learning-development-with-gitlab-ci/){:data-ga-name="Machine learning"}{:data-ga-location="body"} or artificial intelligence. Take the time before rolling it out to consider what your team might like to experiment with.

                6. A *“minimum viable product”* is a worthy goal (well, we think so at GitLab) and it means the product is ready for prime time, but just ready. Considering an “MVP” mindset makes sense as you also consider a DevOps platform – the platform will help teams get to an MVP sooner and the other bonus of an MVP is teams can continue to iterate on it until it’s completely ideal.

                7. Finally, a unified DevOps platform allows unique *visibility and traceability* through the entire software development lifecycle. Make sure your team and all other software development stakeholders take advantage of these features.
    - name: copy-resources
      data:
        title: Resources
        block:
          - text: |
                  Here’s a list of resources on DevOps platforms. This is a new and growing area, so please share your favorites with us via Twitter [@gitlab](https://twitter.com/gitlab).
            resources:
              video:
                header: Videos
                links:
                  - text: How a DevOps platform makes everything simple
                    link: https://www.youtube.com/watch?v=TUwvgz-wsF4
                  - text: Why it's important to integrate monitoring and deployment
                    link: https://www.youtube.com/watch?v=ihdxpO5rgSc
                  - text: Why continuous testing matters
                    link: https://www.youtube.com/watch?v=tQy0O1EGixs
                  - text: How to simplify DevOps
                    link: https://www.youtube.com/watch?v=TUwvgz-wsF4
              case_study:
                header: Case Studies
                links:
                  - text: Goldman Sachs streamlines its DevOps efforts
                    link: /customers/goldman-sachs/
                  - text: BI Worldwide discovers the benefits of a single tool
                    link: /customers/bi_worldwide/
                  - text: The European Space Agency and DevOps
                    link: /customers/european-space-agency/
                  - text: Axway leverages the benefits of a DevOps platform
                    link: /customers/axway-devops/
              report:
                header: Reports
                links:
                  - text: GitLab’s 2020 Global DevSecOps Survey
                    link: /developer-survey/
              blog:
                header: Blog
                links:
                  - text: Gartner on application release orchestration
                    link: /blog/2020/01/16/2019-gartner-aro-mq/

