// File used to prevent generating routes for content files which do not describe pages
// This is done to avoid errors during yarn generate

// eslint-disable-next-line import/no-default-export
export default [
  // eslint-disable-line import/no-default-export
  'content/free-trial-cta.yml',
  'content/next-step.yml',
  'content/navigation.yml',
  'content/footer.yml',
  // Sunsetted pages we keep to turn on when needed
  'content/livestream.yml',
  // Landing pages:
  // These pages have children pages already migrated but an error pops up in the console as nuxt expect the parent to exist
  // These exceptions should be removed when migrating the landing page.
  'content/install',
  'content/solutions',
  'content/partners/technology-partners',
  'content/stages-devops-lifecycle',
  'content/company/culture',
];
